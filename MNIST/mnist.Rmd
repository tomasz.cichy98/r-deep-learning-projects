---
title: 'MNIST R Deep Learning'
author: 'Tomasz Cichy'
date: '`r Sys.Date()`'
output:
  html_document:
    code_folding: show
    number_sections: yes
    toc: yes
    toc_float: true
    df_print: paged
  pdf_document:
    toc: yes
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message=FALSE, warning=FALSE)
```

# Introduction
This is a project from *[R Deep Learning Projects](https://www.packtpub.com/big-data-and-business-intelligence/r-deep-learning-projects)* book.
Data from [Kaggle digit recogniser](https://www.kaggle.com/c/digit-recognizer/data).

## Load libs
```{r}
library(caret)
library(nnet)
library(magrittr)
library(formattable)
library(keras)
library(tensorflow)
library(viridis)
```


## Load Data
```{r}
data <- read.csv("./data/train.csv")
dim(data)
```

## Set Python
```{r}
library(reticulate)
use_python("/usr/bin/python3.7")
```


Each digit is 28*28 = 784

# Explore
```{r}
head(data)
unique(unlist(data[1]))
paste("Pixel variable represents brightness from", min(data), "to", max(data),".")
```

## Plot
Plot the data to see how the digits look like
```{r}
sample_4 <- matrix(as.numeric(data[4, -1]), nrow = 28, byrow = TRUE)
image(sample_4, col = grey.colors(255))

sample_7 <- matrix(as.numeric(data[7, -1]), nrow = 28, byrow = TRUE)
image(sample_7, col = grey.colors(255))
```
We can rotate them using by reversing elements in each column. Transpose and flip upside down.
```{r}
rotate <- function(x) t(apply(x, 2, rev))
image(rotate(sample_4), col = grey.colors(255))
image(rotate(sample_7), col = grey.colors(255))
```

# Labels and features
## Labels
```{r}
data$label <- as.factor(data$label)
summary(data$label)
proportion <- prop.table(table(data$label)) * 100
cbind(count = table(data$label), proportion = proportion)
```
Classes are well balanced.

## Features
### Central block
Check the distribution of brightness in the middle of the frame.

**par**

> par can be used to set or query graphical parameters. Parameters can be set by specifying them as arguments to par in tag = value form, or by passing them as a list of tagged values.

**sprintf**

> A wrapper for the C function sprintf, that returns a character vector containing a formatted combination of text and variable values.

```{r}
central_block <- c("pixel376", "pixel377", "pixel404", "pixel405")
par(mfrow=c(2, 2))
for (i in 1:9) {
  hist(c(as.matrix(data[data$label == i, central_block])),
       main = sprintf("Histogram for digit %d", i),
       xlab = "Pixel value")
}
```
Number 8 is usually drew with the cross being in the middle of the frame, therefore, majority of the central pixels for digit 8 will be bright.

# Classification

**creareDataPartition**

> A series of test/training partitions are created using createDataPartition while createResample creates one or more bootstrap samples. createFolds splits the data into k groups while createTimeSlices creates cross-validation split for series data. groupKFold splits the data based on a grouping factor.

```{r}
set.seed(1234)
train_perc <- 0.75
train_index <- createDataPartition(data$label, p=train_perc, list = FALSE)

data_train <- data[train_index, ]
data_test <- data[-train_index, ]
```

## Logistic regression

<!-- **multinom** -->

<!-- > Fits multinomial log-linear models via neural networks. -->

<!-- ### Model -->
<!-- ```{r cache=TRUE} -->
<!-- model_lr <- multinom(label ~ ., data=data_train, MaxNWts = 10000, -->
<!--                      decay = 5e-3, maxit = 100) -->
<!-- ``` -->
<!-- **MaxNWts** - maximum of 10000 weights. There are (784 dimensions + 1 bias) * 10 classes = 7850 elements in the weight matrix.   -->
<!-- **decay** - The regularization strength   -->
<!-- **maxit** - max number of iterations   -->

<!-- ### Predict -->
<!-- ```{r} -->
<!-- prediction_lr <- predict(model_lr, data_test, type = "class") -->
<!-- paste("Predicted values:") -->
<!-- prediction_lr[1:5] -->
<!-- paste("Actual values") -->
<!-- data_test$label[1:5] -->
<!-- ``` -->

<!-- ### Eval -->
<!-- ```{r} -->
<!-- cm_lr <- confusionMatrix(prediction_lr, data_test$label) -->
<!-- cm_lr -->

<!-- formattable(cm_lr$table) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- accuracy_lr <- mean(prediction_lr == data_test$label) -->
<!-- paste("Accuracy of the logistic regression model is", round(accuracy_lr, 3) * 100,"%.") -->
<!-- ``` -->

## Keras Fully Connected

> Keras is a high-level neural networks API developed with a focus on enabling fast experimentation. Being able to go from idea to result with the least possible delay is key to doing good research.

### Prapare
Keras does not accept Data Frames so we need to convert the data into a matrix form. It is also necessary to reshape the data into a Keras format.

**array_reshape**

> Reshape (reindex) a multi-dimensional array, using row-major (C-style) reshaping semantics by default.

**to_categorical**

> Converts a class vector (integers) to binary class matrix.


**as.matrix.data.frame**

> matrix creates a matrix from the given set of values.
as.matrix attempts to turn its argument into a matrix.
is.matrix tests if its argument is a (strict) matrix.

```{r}
x_train <- data_train[, -1]
y_train <- data_train[, 1]
x_test <- data_test[, -1]
y_test <- data_test[, 1]

# reshape
x_train <- keras::array_reshape(x_train, c(nrow(x_train), 784))
x_test <- keras::array_reshape(x_test, c(nrow(x_test), 784))

# rescale
x_train <- x_train/255
x_test <- x_test/255

# categorical one hot encode
y_train <- keras::to_categorical(y_train, 10)
y_test <- keras::to_categorical(y_test, 10)

# keras does not accept dataframes
x_train <- as.matrix.data.frame(x_train)
x_test <- as.matrix.data.frame(x_test)
```
Scale input form to a range from 0 to 1.

### Model
```{r}
model_keras <- keras_model_sequential()
model_keras %>%
  layer_dense(units = 256, activation = "relu", input_shape = c(784)) %>%
  layer_dropout(rate = 0.4) %>%
  layer_dense(units = 64, activation = "relu") %>%
  layer_dropout(rate = 0.3) %>%
  layer_dense(units = 10, activation = "softmax")

summary(model_keras)
```
**input_shape** - there are 784 pixels in the image  
**units** - number of units per layer

```{r}
model_keras %>% compile(
  loss = "categorical_crossentropy",
  optimizer = optimizer_rmsprop(),
  metrics = c('accuracy')
)
```

### Train
Commented because I can just load it.
```{r}
# history <- model_keras %>% fit(
#   x_train, y_train,
#   epochs = 30, batch_size = 100,
#   validation_split = 0.2
# )
# 
# save_model_hdf5(model_keras, filepath = "model_keras.h5")
```
**epochs** - number of repetitions  
**batch_size** - For a mini-batch gradient descent. Calculates costs and gradients by small batches instead of entire samples. It is more efficient.

```{r}
model_keras <- load_model_hdf5("model_keras.h5")
```

### Predict
```{r echo=FALSE}
predictions_ker <- model_keras %>% predict_classes(x_test)
predictions_ker <- as.factor(predictions_ker)
paste("Predicted values:")
predictions_ker[1:5]
paste("Actual values:")
data_test[1:5,1]
```

### Eval
```{r echo=FALSE}
keras_acc <- model_keras %>% evaluate(x_test, y_test)
paste("Accuracy of the Keras model is:", round(keras_acc$acc, 3) * 100, "%")

keras_cm <- confusionMatrix(predictions_ker, data_test[, 1])

ggplot(data = as.data.frame(keras_cm$table), aes(x = Prediction, y = Reference, fill = Freq)) +
  geom_tile() + geom_text(aes(label = Freq)) +
  scale_fill_viridis() + theme_minimal() +
  ggtitle("Keras Model Confusion Matrix")
```
By adding 1 layer I was able to reach 97% accuracy and it still can be improved.

## Keras CNN
> CNNs use a variation of multilayer perceptrons designed to require minimal preprocessing. They are also known as shift invariant or space invariant artificial neural networks (SIANN), based on their shared-weights architecture and translation invariance characteristics.
Convolutional networks were inspired by biological processes in that the connectivity pattern between neurons resembles the organization of the animal visual cortex. Individual cortical neurons respond to stimuli only in a restricted region of the visual field known as the receptive field. The receptive fields of different neurons partially overlap such that they cover the entire visual field. 

![CNN Diagram](./images/cnn_diagram.png)

### Prepare
CNN network takes in a tensor shaped (28 x 28 x 1) so we need to create an array full of these tensors.
```{r}
x_train <- data_train[, -1]
y_train <- data_train[, 1]
x_test <- data_test[, -1]
y_test <- data_test[, 1]

x_train <- as.matrix.data.frame(x_train)
x_test <- as.matrix.data.frame(x_test)

# reshapre
x_train <- keras::array_reshape(x_train, c(nrow(x_train), 28, 28, 1))
x_test <- keras::array_reshape(x_test, c(nrow(x_test), 28, 28, 1))

# rescale
x_train <- x_train / 255
x_test <- x_test / 255

# one hot encode
y_train <- keras::to_categorical(y_train, 10)
y_test <- keras::to_categorical(y_test, 10)

cat('x_train_shape:', dim(x_train), '\n')
cat('x_test_shape:', dim(x_train), '\n')
cat(nrow(x_train), 'train samples\n')
cat(nrow(x_test), 'test samples\n')
```


### Model
Output of the first layer will be 20 \* (28 - 5 + 1) \* (28 -5 + 1) = 20 \* 24 \* 24 = 11520.
Than we apply 50 5\*5 filters to get 50 \* 20 \* (24 - 5 + 1) \* (24 - 5 + 1) = 400000.
And we have started with 784.
That's why we use max_pooling layers. They down-sample conv layers by grouping their output into regions and selecting max or median of the region.

```{r}
model_cnn <- keras_model_sequential()
model_cnn %>%
  layer_conv_2d(filters = 20, kernel_size = c(5, 5), activation = "relu", input_shape = c(28, 28, 1)) %>%
  layer_max_pooling_2d(strides = c(2, 2), pool_size = c(2, 2)) %>%
  layer_conv_2d(kernel_size = c(5, 5), filters = 50, activation = "relu") %>%
  layer_max_pooling_2d(strides = c(2, 2), pool_size = c(2, 2)) %>%
  layer_flatten() %>%
  layer_dense(units = 500, activation = "relu") %>%
  layer_dense(units = 10, activation = "softmax")

summary(model_cnn)
```
First we create a conv layer with 20 5\*5 filters and "relu" activation. We pool it with a 2\*2 filter.
Next we create a conv layer with 50 5\*5 filters and "relu" activation. We pool it with a 2\*2 filter.
Now we flatten the output in order to add fully connected layers.

So in our case the model will look more or less like this one
![CNN 5](./images/cnn_5.png)

```{r}
model_cnn %>% compile(
  loss = "categorical_crossentropy",
  optimizer = optimizer_adadelta(lr = 0.05, decay = 0.00001),
  metrics = c("accuracy")
)
```

### Train
```{r}
# history <- model_cnn %>% fit(
#   x_train, y_train,
#   batch_size = 100,
#   epochs = 30,
#   validation_split = 0.2
# )
# 
# save_model_hdf5(model_cnn, "model_cnn.h5")
# 
# plot(history)
```
```{r}
model_cnn <- load_model_hdf5("model_cnn.h5")
```


### Predict and Eval
```{r echo=FALSE}
predictions_cnn <- model_cnn %>% predict_classes(x_test)
predictions_cnn <- as.factor(predictions_cnn)
paste("Predicted values:")
predictions_cnn[1:5]
paste("Actual values:")
data_test[1:5,1]
```

```{r echo=FALSE}
accuracy_cnn <- model_cnn %>% evaluate(x_test, y_test)

paste("Accuracy of the Keras CNN model is:", round(accuracy_cnn$acc, 3) * 100, "%")

keras_cm <- confusionMatrix(predictions_cnn, data_test[, 1])

ggplot(data = as.data.frame(keras_cm$table), aes(x = Prediction, y = Reference, fill = Freq)) +
  geom_tile() + geom_text(aes(label = Freq)) +
  scale_fill_viridis() + theme_minimal() +
  ggtitle("Keras CNN Model Confusion Matrix")
```

### Visual Activations
#### Reference
```{r}
par(mfrow = c(1, 2))
test_1 <- matrix(as.numeric(data_test[1, -1]), nrow = 28, byrow = TRUE)
image(rotate(test_1), col = gray.colors(255))
test_2 <- matrix(as.numeric(data_test[2, -1]), nrow = 28, byrow = TRUE)
image(rotate(test_2), col = gray.colors(255))
```

#### Generic functions
```{r}
plot_channel <- function(channel) {
  image(rotate(channel), axes = FALSE, asp = 1, 
        col = terrain.colors(12))
}

vis_acti <- function(n){
  x_act <- data_test[data_test$label == n, -1]
  y_act <- data_test[data_test$label == n, 1]
  
  # Prepare the data
  x_act <- as.matrix.data.frame(x_act)
  x_act <- keras::array_reshape(x_act, c(nrow(x_act), 28, 28, 1))
  x_act <- x_act/255
  
  # prepare model
  layer_outputs <- lapply(model_cnn$layers[1:4], function(layer) layer$output)
  activation_model_cnn <- keras_model(inputs = model_cnn$input, outputs = layer_outputs)
  
  # make it predicts for a given number to trigger activations
  activations <- activation_model_cnn %>% predict(x_act)

  # check dimensions
  first_layer_act <- activations[[1]]
  dim(first_layer_act)
  
  
  #iterate
  image_size <- 58
  images_per_row <- 8

  for (i in 1:4) {
    layer_activation <- activations[[i]]
    layer_name <- model_cnn$layers[[i]]$name
   
    print(paste("==== Activations for layer:", i, "==== Layer name:", layer_name))

    n_features <- dim(layer_activation)[[4]]
    n_cols <- n_features %/% images_per_row
    # 
    # png(paste0("1_activations/", i, "_", layer_name, ".png"), 
    #     width = image_size * images_per_row, 
    #     height = image_size * n_cols)
    op <- par(mfrow = c(n_cols, images_per_row), mai = rep_len(0.02, 4))
    
    for (col in 0:(n_cols-1)) {
      for (row in 0:(images_per_row-1)) {
        channel_image <- layer_activation[1,,,(col*images_per_row) + row + 1]
        plot_channel(channel_image)
      }
    }
    
    par(op)
    # dev.off()
  }
}
```

#### Visualisations
We can visualise activations of each of the conv and pooling layers.  
1-2 row - 1st conv layer  
3-4 row - 1st pooling layer (down-sampled)  
5-10 rows - 2nd conv layer  
11-16 rows - 2nd pooling layer (down-sampled)
```{r}
vis_acti(1)
```

# References and links

1. https://www.packtpub.com/big-data-and-business-intelligence/r-deep-learning-projects
1. https://rstudio-pubs-static.s3.amazonaws.com/377864_e02aef8c09a048f1bb25f0bb40770221.html
1. https://en.wikipedia.org/wiki/Convolutional_neural_network
1. https://towardsdatascience.com/a-comprehensive-guide-to-convolutional-neural-networks-the-eli5-way-3bd2b1164a53
1. https://towardsdatascience.com/convolutional-neural-networks-from-the-ground-up-c67bb41454e1




















